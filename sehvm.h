#pragma once
/*SEH based VM Engine by Yattering, 2016
 *e-mail: yattering (at) sigaint (d0t) org
 *jabber: yattering (at) xmpp (d0t) jp
 */
#define SEHVM_TYPE_RET_C8 0x0
#define SEHVM_TYPE_PUSH_C32 0x1
#define SEHVM_TYPE_PUSH_R32 0x2
#define SEHVM_TYPE_POP_R32 0x3
#define SEHVM_TYPE_JMP_REL_C32 0x4
#define SEHVM_TYPE_CALL_REL_C32 0x5
#define SEHVM_ARG_EAX 0x0
#define SEHVM_ARG_EBX 0x1
#define SEHVM_ARG_ECX 0x2
#define SEHVM_ARG_EDX 0x3
#define SEHVM_ARG_ESI 0x4
#define SEHVM_ARG_EDI 0x5
#define SEHVM_ARG_EBP 0x6
#define SEHVM_ARG_INVALID_REGISTER 0x7
#define SEHVM_INT3_OPCODE 0xCC

#define SEHVM_PUSH_R32(r32val)\
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_PUSH_R32) __asm _emit(r32val)

#define SEHVM_POP_R32(r32val)\
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_POP_R32) __asm _emit(r32val)

#define SEHVM_NOP() \
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_POP_R32) __asm _emit(SEHVM_ARG_INVALID_REGISTER)

#define SEHVM_JMP_REL_C32(VAL) \
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_JMP_REL_C32) __asm _emit(VAL&0xFF) __asm _emit((VAL>>0x8)&0xFF) __asm _emit((VAL>>0x10)&0xFF) __asm _emit((VAL>>0x18)&0xFF)

#define SEHVM_CALL_REL_C32(VAL) \
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_CALL_REL_C32) __asm _emit(VAL&0xFF) __asm _emit((VAL>>0x8)&0xFF) __asm _emit((VAL>>0x10)&0xFF) __asm _emit((VAL>>0x18)&0xFF)

#define SEHVM_RET_C8(c8val) \
  __asm _emit(SEHVM_INT3_OPCODE) __asm _emit(SEHVM_TYPE_RET_C8) __asm _emit(c8val)

extern EXCEPTION_DISPOSITION __cdecl exception_handler(struct _EXCEPTION_RECORD *except, void * establisher_frame, struct _CONTEXT *context, void *dispatcher_context);
