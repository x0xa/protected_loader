@echo. Piosition-independed Loader by Yattering, 2016
@echo. e-mail: yattering (at) sigaint (d0t) org
@echo. jabber: yattering (at) xmpp (d0t) jp
@SET FASMINC=C:\DEV\FASM\INCLUDE
@CALL C:\DEV\PELLESC\BIN\POVARS32.BAT
@POCC "EXCEPTION_HANDLER.C" /Ze /Os /Tx86-coff /Fo.\EXCEPTION_HANDLER.OBJ
@echo. Compiling EXE...
@echo. output_format = 1 > settings.inc
@echo. __url equ 'https://gitlab.com/yattering/SEH_based_VM/raw/master/demo_fasm.exe' >> settings.inc
@echo. __file equ 'demo.exe' >> settings.inc
@C:\DEV\FASM\FASM.EXE loader.asm loader.obj
@POLINK /SUBSYSTEM:WINDOWS /OUT:loader.exe loader.obj exception_handler.obj /ENTRY:main
@echo. Compiling BIN...
@echo. output_format = 0 > settings.inc
@echo. __url equ 'https://gitlab.com/yattering/SEH_based_VM/raw/master/demo_fasm.exe' >> settings.inc
@echo. __file equ 'demo.exe' >> settings.inc
@C:\DEV\FASM\FASM.EXE loader.asm
@POLINK /SUBSYSTEM:WINDOWS /OUT:loader.temp /ENTRY:main loader.obj exception_handler.obj
@C:\DEV\CYGWIN64\BIN\i686-w64-mingw32-objcopy.exe -S -R .rdata -O binary loader.temp loader.bin